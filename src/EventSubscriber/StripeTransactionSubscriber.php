<?php

namespace Drupal\commerce_payment_fee_stripe\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\commerce_stripe\Event\StripeEvents;
use Drupal\commerce_stripe\Event\TransactionDataEvent;

class StripeTransactionSubscriber implements EventSubscriberInterface {

  /**
   * The minor units converter.
   *
   * @var \Drupal\commerce_price\MinorUnitsConverterInterface
   */
  protected $minorUnitsConverter;

  /**
   * @param \Drupal\commerce_price\MinorUnitsConverterInterface $minorUnitsConverter
   */
  public function __construct(\Drupal\commerce_price\MinorUnitsConverterInterface $minorUnitsConverter) {
    $this->minorUnitsConverter = $minorUnitsConverter;
  }


  public function importStripeFee(TransactionDataEvent $event) {
    $payment = $event->getPayment();
    $payment_intent = \Stripe\PaymentIntent::retrieve($payment->getRemoteId());
    $payment_intent->charges->data[0]->balance_transaction;
    $balance_transaction_id = $payment_intent->charges->data[0]->balance_transaction;
    $balance_transaction = \Stripe\BalanceTransaction::retrieve($balance_transaction_id, []);
    $currency_code = $payment->getAmount()->getCurrencyCode();
    $fee_amount = $this->minorUnitsConverter->fromMinorUnits($balance_transaction->fee,$currency_code);
    $net_amount = $this->minorUnitsConverter->fromMinorUnits($balance_transaction->net, $currency_code);
    $payment->set('commerce_payment_fee_amount', ['number' => $fee_amount->getNumber(), 'currency_code' => $fee_amount->getCurrencyCode()]);
    $payment->set('commerce_payment_fee_net_amount', ['number' => $net_amount->getNumber(), 'currency_code' => $net_amount->getCurrencyCode()]);
    $payment->save();
  }

  public static function getSubscribedEvents() {
    return [
      StripeEvents::TRANSACTION_DATA => ['importStripeFee', -1000],
    ];
  }


}
